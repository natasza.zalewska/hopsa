WIDTH = 1000
HEIGHT = 650
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
FPS = 50
SPRITESHEET = "sprite.png"

PLAYER_ACC = 1
PLAYER_FRICTION = -0.12
PLAYER_GRAV = 0.8
PLAYER_JUMP = -20

PLATFORM_LIST = [(0, HEIGHT - 40),
                 (WIDTH / 2 - 50, HEIGHT * 2.5 / 4),
                 (125, HEIGHT - 350),
                 (350, 200),
                 (175, 100),
                 (580, 50)

                 ]